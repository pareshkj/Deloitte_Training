package javaTestQ2;

public class EmployeeVo {
	int employeeId;
	String employeeName;
	int annualIncome;
	int incomeTax;
	
	
	public EmployeeVo() {}
	
	public EmployeeVo(int employeeId, String employeeName, int annualIncome, int incomeTax) {
		super();
		this.employeeId = employeeId;
		this.employeeName = employeeName;
		this.annualIncome = annualIncome;
		this.incomeTax = incomeTax;
	}

	public int getemployeeId() {
		return employeeId;
	}
	public String getemployeeName() {
		return employeeName;
	}
	public int getAnnualIncome() {
		return annualIncome;
	}
	public int getIncomeTax() {
		return incomeTax;
	}
	public void setemployeeId(int employeeId) {
		this.employeeId = employeeId;
	}
	public void setemployeeName(String employeeName) {
		this.employeeName = employeeName;
	}
	public void setAnnualIncome(int annualIncome) {
		this.annualIncome = annualIncome;
	}
	public void setIncomeTax(int incomeTax) {

		this.incomeTax = incomeTax;
	}
	
		
	
	@Override
	public String toString() {
		return "EmployeeVo [employeeId=" + employeeId + ", employeeName=" + employeeName + ", annualIncome=" + annualIncome + ", incomeTax="
				+ incomeTax + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + annualIncome;
		result = prime * result + employeeId;
		result = prime * result + ((employeeName == null) ? 0 : employeeName.hashCode());
		result = prime * result + incomeTax;
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EmployeeVo other = (EmployeeVo) obj;
		if (annualIncome != other.annualIncome)
			return false;
		if (employeeId != other.employeeId)
			return false;
		if (employeeName == null) {
			if (other.employeeName != null)
				return false;
		} else if (!employeeName.equals(other.employeeName))
			return false;
		if (incomeTax != other.incomeTax)
			return false;
		return true;
	}
	
}
