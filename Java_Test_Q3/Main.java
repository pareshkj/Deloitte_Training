package javaTestQ3;

public class Main {
	public static void main(String[] args) {		
		int n = Integer.parseInt(args[0]);
		int[] x = new int[n];
		for (int i=0; i<n; i++) {
			x[i] = Integer.parseInt(args[i+1]);
		}
		System.out.println(UserMainCode.checkTriplets(x) ? "TRUE" : "FALSE" );
	}
}
