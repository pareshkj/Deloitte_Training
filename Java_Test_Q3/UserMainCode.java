package javaTestQ3;

public class UserMainCode {

	public static boolean checkTriplets(int[] array) {
		
		int num = Integer.MIN_VALUE,count = 0;
		
		for(int i: array) {
			
			if(num==i) 
				count++;
			else {
				num=i;
				count=1;
			}
			
			if(count==3) 
				return true;
		}
		return false;
	}


}
