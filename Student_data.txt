insert into s_details values(1, 'rohit', 'ranjan', 2, null, null, '01-jan-1994', 82883);
insert into s_details values(2, 'rahul', 'raj', 3, null, null, '27-nov-1997', 4545);
insert into s_details values(3, 'noel', 'sam', 32, null, null, '02-sep-1998', 2344);
insert into s_details values(4, 'kp', 'vishnu', 45, null, null, '24-oct-1994', 5645);
insert into s_details values(5, 'paresh', 'kg', 54, null, null, '10-aug-1993', 8788);
insert into s_details values(6, 'barjindar', 'kaur', 323, null, null, '24-apr-1990', 6566);
insert into s_details values(7, 'shreya', 'kumari', 326, null, null, '24-feb-1991', 4565);
insert into s_details values(8, 'mohit', 'rautela', 65, null, null, '14-sep-1994', 23445);
insert into s_details values(9, 'priyanka', 'sen', 284, null, null, '22-dec-1996', 3466);
insert into s_details values(10, 'samuel', 'james', 385, null, null, '13-feb-1993', 2355);


insert into state values(1, 'karnataka');
insert into state values(2, 'tamil nadu');
insert into state values(3, 'haryana');
insert into state values(4, 'delhi');
insert into state values(5, 'karnataka');
insert into state values(6, 'gujrat');
insert into state values(7, 'maharastra');
insert into state values(8, 'kerala');
insert into state values(9, 'bihar');
insert into state values(10, 'west bengal');



insert into city values(1, 1, 'bangalore');
insert into city values(2, 2, 'chennai');
insert into city values(3, 3, 'punjab');
insert into city values(4, 4, 'delhi');
insert into city values(5, 5, 'bangalore');
insert into city values(6, 6, 'surat');
insert into city values(7, 7, 'mumbai');
insert into city values(8, 8, 'tiruwanantpuram');
insert into city values(9, 9, 'patna');
insert into city values(10, 10, 'kolkata');

insert into marks values(1, 1, 50, 60, 70, 1);
insert into marks values(323, 2, 68, 78, 70, 1);
insert into marks values(24, 3, 45, 78, 70, 1);
insert into marks values(435, 4, 77, 37, 70, 1);
insert into marks values(35, 5, 57, 68, 70, 1);
insert into marks values(124, 6, 87, 23, 70, 1);
insert into marks values(546, 7, 75, 98, 70, 1);
insert into marks values(243, 8, 56, 63, 70, 1);
insert into marks values(436, 1, 35, 60, 70, 2);
insert into marks values(499, 3, 55, 35, 35, 2);
insert into marks values(500, 4, 58, 57, 46, 2);
insert into marks values(501, 7, 23, 57, 56, 2);
insert into marks values(502, 10, 78, 23, 23, 2);


insert into sub values(1,'maths');
insert into sub values(2,'science');
insert into sub values(3,'english');
insert into sub values(4,'hindi');
insert into sub values(5,'kannada');
insert into sub values(6,'social science');

insert into conn values(1, 2, 2, 4, 1);
insert into conn values(2, 4, 3, 6, 2);
insert into conn values(3, 6, 4, 2, 2);
insert into conn values(4, 2, 2, 3, 4);
insert into conn values(5, 1, 1, 2, 5);
insert into conn values(6, 7, 5, 1, 3);
insert into conn values(7, 3, 3, 5, 2);
insert into conn values(8, 5, 2, 2, 3);
insert into conn values(9, 4, 1, 4, 4);
insert into conn values(10, 8, 4, 1, 2);

