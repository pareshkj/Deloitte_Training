package javaTestQ1;
import java.util.Scanner;

public class Calc {
	
	private static Scanner input;

	public static void main(String[] args) {			
			Arithmetic a[] =new Arithmetic[4];
			
			
			a[0]= new Addition();
			a[1]= new Subtraction();
			a[2]= new Multiplication();
			a[3]= new Division();
			
			input = new Scanner(System.in);
			System.out.println("Enter number 1 : ");
			double num1 = input.nextDouble();
			System.out.println("Enter number 2 : ");
			double num2 = input.nextDouble();
			System.out.println("Enter choice : \n\t 1. Add \n\t 2. Sub \n\t 3. Multiply \n\t 4. Divide");
			int ch = input.nextInt();
			
			a[0].get(num1, num2);
			a[1].get(num1, num2);
			a[2].get(num1, num2);
			a[3].get(num1, num2);
			
			a[0].calculate();
			a[1].calculate();
			a[2].calculate();
			a[3].calculate();
			
			try {
				a[--ch].display();
			} catch (Exception e) {				
				System.out.println("Invalid Choice");
			}
	
			
		
	}
}